package cmd

import (
 "fmt"
 "os"
 "github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
    Use:  "lowi",
    Short: "lowi - my command-line toolbox",
    Long: `
    _____________
   < hello world >
    -------------
	My command line toolbox
`,
    Run: func(cmd *cobra.Command, args []string) {

    },
}

func Execute() {
    if err := rootCmd.Execute(); err != nil {
        fmt.Fprintf(os.Stderr, "Whoops. There was an error while executing your CLI '%s'", err)
        os.Exit(1)
    }
}