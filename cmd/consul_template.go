/*
Copyright © 2022 Lowi <hello@lowi.dev>
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/lowi/cli/pkg/consul_template"
)

// caCmd represents the pki command
var consulTemplateCmd = &cobra.Command{
	Use:     "consul-template",
	Aliases: []string{"ct", "template"},
	Short:   "All things Consul Template (https://github.com/hashicorp/consul-template)",
	Long: `
____ ____ _  _ ____ _  _ _       ___ ____ _  _ ___  _    ____ ___ ____ 
|    |  | |\ | [__  |  | |        |  |___ |\/| |__] |    |__|  |  |___ 
|___ |__| | \| ___] |__| |___     |  |___ |  | |    |___ |  |  |  |___ 

This one is for setting up Consul template services.
`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("pki called")
	},
}

var deployCmd = &cobra.Command{
	Use:   "deploy",
	Short: "Deploy a Consul Template service instance",
	Long: `
'pxl consul-template deploy' creates a new Consul Template service.
I.e. it starts a systemd service that runs Consul template with a given configuration. 
`,
	Run: func(cmd *cobra.Command, args []string) {
		consul_template.Deploy(name, path, configurationBase64, templatesBase64)
	},
}

var name string
var path string
var templatesBase64 string
var configurationBase64 string

// var creditAmount int64

func init() {
	rootCmd.AddCommand(consulTemplateCmd)

	// Deploy
	// ————————————————————————————————
	consulTemplateCmd.AddCommand(deployCmd)
	deployCmd.Flags().StringVarP(&name, "name", "n", "", "Namespace for Consul Template resources")
	deployCmd.Flags().StringVarP(&path, "path", "p", "/opt/consul-template", "Base installation path")
	deployCmd.Flags().StringVarP(&templatesBase64, "templates", "t", "", "Base64 encoded list of template files.")
	deployCmd.Flags().StringVarP(&configurationBase64, "configuration", "c", "", "Base64 encoded configuration file")


	// generateRootCa.Flags().StringVarP(&secrectsPath, "secrects-path", "p", "", "Base path for secrets. Defaults to `~/.pxl/secrets`")
	// generateRootCa.Flags().Int64VarP(&creditAmount, "amount", "a", 0, "Amount to be credited")
	// generateRootCa.MarkFlagRequired("narration")
	// generateRootCa.MarkFlagRequired("amount")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// caCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// caCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
