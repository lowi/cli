/*
Copyright © 2022 Lowi <hello@lowi.dev>
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/lowi/cli/pkg/tui"
)

// caCmd represents the pki command
var tuiCmd = &cobra.Command{
	Use:     "tui",
	Short:   "Start TUI application",
	Long: `
Start TUI application
`,
	Run: func(cmd *cobra.Command, args []string) {
		tui.Run()
	},
}

func init() {
	rootCmd.AddCommand(tuiCmd)
}
