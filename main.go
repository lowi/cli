/*
Copyright © 2022 Lowi <hello@lowi.dev>
*/
package main

import "gitlab.com/lowi/cli/cmd"

func main() {
	cmd.Execute()
}
