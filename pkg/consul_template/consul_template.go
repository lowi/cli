package consul_template

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
)

func Map[T, V any](ts []T, fn func(T) V) []V {
	result := make([]V, len(ts))
	for i, t := range ts {
		result[i] = fn(t)
	}
	return result
}

type TemplateFile struct {
	Name    string `json:"name"`
	Content string `json:"content"`
	// Command string `json:"cmd"`
	// Path    string `json:"dest"`
}

func Deploy(
	name string,
	path string,
	configurationBase64 string,
	templatesBase64 string,
) error {
	files, err := decodeTemplateFiles(templatesBase64)
	if err != nil {
		panic(err)
	}
	fmt.Printf("files: %s", files)

	configuration, err := decodeConfiguration(configurationBase64)
	if err != nil {
		panic(err)
	}
	fmt.Printf("configuration: %s", configuration)

	return nil
}

func decodeConfiguration(configurationBase64 string) (string, error) {
	// Decode from base64, which gives us the file content
	configuration, err := base64.StdEncoding.DecodeString(configurationBase64)
	if err != nil {
		panic(err)
	}
	return string(configuration), nil
}

func decodeTemplateFiles(templatesBase64 string) ([]TemplateFile, error) {

	// Decode from base64, which gives us a JSON string
	templatesJson, err := base64.StdEncoding.DecodeString(templatesBase64)
	if err != nil {
		panic(err)
	}

	// Unmarshal JSON
	var templates []TemplateFile
	errr := json.Unmarshal(templatesJson, &templates)
	if err != nil {
		panic(errr)
	}

	// The .Content of all template files are Base64 ecoded
	mapped := Map(templates, func(template TemplateFile) TemplateFile {
		content, err := base64.StdEncoding.DecodeString(template.Content)
		if err != nil {
			panic(err)
		}
		return TemplateFile{
			Name:    template.Name,
			Content: string(content),
		}
	})

	return mapped, nil
}
